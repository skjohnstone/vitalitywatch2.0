package com.example.scott.vitalitywatch2.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.NumberPicker;

import com.example.scott.vitalitywatch2.Activities.MainActivity;

/**
 * Created by sjohn on 19/11/2017.
 */

public class SingleInputFragment extends DialogFragment {
    private String title;
    private String description;
    private int currentValue;
    private int minValue;
    private int maxValue;
    private int stepSize;
    private int id;

    public void setDetails(String title, String description, int currentValue, int minValue, int maxValue, int stepSize, int id) {
        this.title = title;
        this.description = description;
        this.currentValue = currentValue;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.stepSize = stepSize;
        this.id = id;

    }

    public Dialog onCreateDialog (Bundle savedInstanceState) {
        final NumberPicker numberPicker = new NumberPicker(getActivity());
        numberPicker.setMaxValue(maxValue);
        numberPicker.setMinValue(minValue);
        numberPicker.setValue(currentValue);
        if (stepSize != 1) {
            String[] valueSet = new String[(maxValue + stepSize) / (minValue + stepSize)];
            for (int i = minValue; i <= maxValue; i++) {
                valueSet[i] = String.valueOf(i * stepSize);
            }
            numberPicker.setDisplayedValues(valueSet);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setMessage(description)
                .setView(numberPicker)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity callingActivity = (MainActivity) getActivity();
                        callingActivity.onDialogOK(numberPicker, null, id);
                        //valueChangeListener.onValueChange(numberPicker, id, numberPicker.getValue());
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        return builder.create();
    }
}
