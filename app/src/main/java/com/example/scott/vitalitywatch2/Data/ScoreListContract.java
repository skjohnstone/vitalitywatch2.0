package com.example.scott.vitalitywatch2.Data;

import android.provider.BaseColumns;

/**
 * Created by Scott on 03/12/2017.
 */


public class ScoreListContract {

    public static final class ScoreListEntry implements BaseColumns {
        public static final String TABLE_NAME = "scorelist";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_SCORE = "score";

        public static final String COLUMN_ALCOHOL_SCORE = "alcoholScore";
        public static final String COLUMN_ALCOHOL_DRINKS = "alcoholDrinks";

        public static final String COLUMN_BLOODS_SCORE = "bloodScore";
        public static final String COLUMN_SYSTOLIC = "bloodSystolic";
        public static final String COLUMN_DIASTOLIC = "bloodDiastolic";

        public static final String COLUMN_DESTRESS_SCORE = "deStressScore";
        public static final String COLUMN_STRESS_LEVEL = "stressLevel";

        public static final String COLUMN_DETOX_SCORE = "deToxScore";
        public static final String COLUMN_CIGARETTES = "cigarettes";

        public static final String COLUMN_DIET_SCORE = "dietScore";
        public static final String COLUMN_DIET_LEVEL = "dietLevel";

        public static final String COLUMN_EXERCISE_SCORE = "exerciseScore";
        public static final String COLUMN_STEPS = "steps";
        public static final String COLUMN_MINS = "mins";

        public static final String COLUMN_FIVE_A_DAY_SCORE = "fiveADayScore";
        public static final String COLUMN_NO_FRUIT_VEG = "fruitVeg";

        public static final String COLUMN_HAPPY_HOURS_SCORE = "happyHoursScore";
        public static final String COLUMN_HAPPY_HOURS = "happyHours";

        public static final String COLUMN_SLEEP_SCORE = "sleepScore";
        public static final String COLUMN_HOURS_SLEEP = "sleepHours";

        public static final String COLUMN_SUGAR_SALT_SCORE = "sugarSaltScore";
        public static final String COLUMN_SUGAR = "sugar";
        public static final String COLUMN_SALT = "salt";

        public static final String COLUMN_WATER_SCORE = "waterScore";
        public static final String COLUMN_GLASSES = "glasses";

        public static final String COLUMN_WEIGHT_SCORE = "weightScore";
        public static final String COLUMN_BMI = "bmi";
        public static final String COLUMN_WEIGHT = "weight";
        public static final String COLUMN_HEIGHT = "height";
    }


}
