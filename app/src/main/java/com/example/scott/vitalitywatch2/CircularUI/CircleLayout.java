package com.example.scott.vitalitywatch2.CircularUI;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.RelativeLayout;

import java.util.ArrayList;

/**
 * Created by sjohn on 02/11/2017.
 */

public class CircleLayout extends RelativeLayout implements CircleButtonLayoutAdapter.CircularItemChangeListener {

    private CircularTouchListener circularTouchListener;
    public float itemWidth = 0;
    public float itemHeight = 0;
    public float layoutWidth;
    public float layoutHeight;
    public float viewWidth;
    public float viewHeight;
    public float layoutCenter_x;
    public float layoutCenter_y;
    public float radius;
    public float diameter;
    private double intervalAngle = Math.PI / 4;
    private double pre_IntervalAngle = Math.PI / 4;
    private double startAngle;
    private double marginLeft;
    private double marginTop;
    private CircleButtonLayoutAdapter adapter;
    public static float MoveAccumulator = 0;

    public CircleLayout(Context context) {
        super(context);
        init();
    }

    public CircleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CircleLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {       //Initialise and get real height and width

        post(new Runnable() {
            @Override
            public void run() {
                Log.e("CircularListView", "get layout width and height");
                layoutWidth = getWidth();
                layoutHeight = getHeight();
                layoutCenter_x = layoutWidth / 2;
                layoutCenter_y = layoutHeight / 2;
                radius = (layoutWidth / 2.6f);
            }
        });
        circularTouchListener = new CircularTouchListener();
        setOnTouchListener(circularTouchListener);
    }

    public double getIntervalAngle() {
        return intervalAngle;
    }

    public void onCircularItemChange() {
        setButtonPositions();
    }

    public void setAdapter(CircleButtonLayoutAdapter adapter) {
        this.adapter = adapter;
        setCircleLayoutBackground();
        adapter.setOnItemChangeListener(this);
        setButtonPositions();
    }

    public void setCircleLayoutBackground() {
        final View view = adapter.getBackground();

        if (view.getParent() == null) {
            addView(view);
        }
        view.post(new Runnable() {
            @Override
            public void run() {
                diameter = (radius * 2.6f);
                LayoutParams params = new LayoutParams(0, 0);
                params.height = ((int) diameter);
                params.width = ((int) diameter);
                params.setMargins((int) (layoutCenter_x - diameter / 2),
                        (int) (layoutCenter_y - diameter / 2), 0, 0);
                view.setLayoutParams(params);
            }
        });
    }

    private void setButtonPositions() {
        int viewCount = adapter.getCount();
        startAngle = -Math.PI / 2f;
        int existChildCount = getChildCount();
        boolean isLayoutEmpty = existChildCount == 0;
        pre_IntervalAngle = isLayoutEmpty ? 0 : 2.0f * Math.PI / (double) existChildCount;
        intervalAngle = 2f * Math.PI / (double) viewCount;

        for (int i = 0; i < adapter.getCount(); i++) {
            final View view = adapter.getViewAt(i);
            final int idx = i;
            final String name = adapter.getNameAt(i);
            if (view.getParent() == null) {
                addView(view);
            }

            view.post(new Runnable() {
                @Override
                public void run() {
                    viewWidth = view.getWidth();
                    viewHeight = view.getHeight();
                    ValueAnimator valueAnimator = new ValueAnimator();
                    valueAnimator.setFloatValues((float) pre_IntervalAngle, (float) intervalAngle);
                    valueAnimator.setDuration(500);
                    valueAnimator.setInterpolator(new OvershootInterpolator());
                    valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                        @Override
                        public void onAnimationUpdate(ValueAnimator animation) {
                            float value = (Float) (animation.getAnimatedValue());
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) view.getLayoutParams();
                            params.setMargins(
                                    (int) (layoutCenter_x - (viewWidth / 2) + (radius *
                                            Math.cos(idx * value + CircleLayout.MoveAccumulator * Math.PI * 2))),
                                    (int) (layoutCenter_y - (viewHeight / 2) + (radius *
                                            Math.sin(idx * value + CircleLayout.MoveAccumulator * Math.PI * 2))), 0, 0);
                            view.setLayoutParams(params);
                        }
                    });
                    valueAnimator.start();
                    view.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    public CircleButtonLayoutAdapter getAdapter() {
        return this.adapter;
    }
}

