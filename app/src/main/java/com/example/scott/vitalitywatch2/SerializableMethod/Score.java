package com.example.scott.vitalitywatch2.SerializableMethod;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by sjohn on 03/11/2017.
 */

public class Score implements Serializable {

    private Date date;
    private int alcoholScore, alcoholUnit;
    private int bloodsScore, systolic, diastolic;
    private int deStressScore, stressLevel;
    private int deToxScore, noCigarettes;
    private int dietScore, dietLevel;
    private int exerciseScore, steps, mins;
    private int fiveADayScore, noFruitVeg;
    private int happyHoursScore, noHappyHours;
    private int sleepScore, noSleepHours;
    private int sugarAndSaltScore, sugar, salt;
    private int waterScore, noGlasses;
    private int weightScore, bmi;

    public Score(Date date) {
        this.date = date;
        this.alcoholScore = 0;
        this.bloodsScore = 0;
        this.deStressScore = 0;
        this.deToxScore = 0;
        this.dietScore = 0;
        this.exerciseScore = 0;
        this.fiveADayScore = 0;
        this.happyHoursScore = 0;
        this.sleepScore = 0;
        this.sugarAndSaltScore = 0;
        this.waterScore = 0;
        this.weightScore = 0;
    }

    public Date getDate() {
        return this.date;
    }

    public int getAlcoholScore() {
        return this.alcoholScore;
    }

    public int getAlcoholUnit() {
        return this.alcoholUnit;
    }

    public int getBloodsScore() {
        return this.bloodsScore;
    }

    public int getSystolic() {
        return this.systolic;
    }

    public int getDiastolic() {
        return this.diastolic;
    }

    public int getDeStressScore() {
        return this.deStressScore;
    }

    public int getStressLevel() {
        return this.stressLevel;
    }

    public int getDeToxScore() {
        return this.deToxScore;
    }

    public int getNoCigarettes() {
        return this.noCigarettes;
    }

    public int getDietScore() {
        return this.dietScore;
    }

    public int getDietLevel() {
        return this.dietLevel;
    }

    public int getExerciseScore() {
        return this.exerciseScore;
    }

    public int getSteps() {
        return this.steps;
    }

    public int getMins() {
        return this.mins;
    }

    public int getFiveADayScore() {
        return this.fiveADayScore;
    }

    public int getNoFruitVeg() {
        return this.noFruitVeg;
    }

    public int getHappyHoursScore() {
        return this.happyHoursScore;
    }

    public int getNoHappyHours() {
        return this.noHappyHours;
    }

    public int getSleepScore() {
        return this.sleepScore;
    }

    public int getNoSleepHours() {
        return this.noSleepHours;
    }

    public int getSugarAndSaltScore() {
        return this.sugarAndSaltScore;
    }

    public int getSugar() {
        return this.sugar;
    }

    public int getSalt() {
        return this.salt;
    }

    public int getWaterScore() {
        return this.waterScore;
    }

    public int getNoGlasses() {
        return this.noGlasses;
    }

    public int getWeightScore() {
        return this.weightScore;
    }

    public int getBmi() {
        return this.bmi;
    }


    public ArrayList<Integer> getAllScores() {
        ArrayList<Integer> returnThis = new ArrayList<>();
        returnThis.add(this.alcoholScore);
        returnThis.add(this.bloodsScore);
        returnThis.add(this.deStressScore);
        returnThis.add(this.deToxScore);
        returnThis.add(this.dietScore);
        returnThis.add(this.exerciseScore);
        returnThis.add(this.fiveADayScore);
        returnThis.add(this.happyHoursScore);
        returnThis.add(this.sleepScore);
        returnThis.add(this.sugarAndSaltScore);
        returnThis.add(this.waterScore);
        returnThis.add(this.weightScore);
        return returnThis;
    }

    public int getTotalScore() {
        int sum = 0;
        ArrayList<Integer> list = getAllScores();
        for (int i = 0; i < list.size(); i++) {
            sum += list.get(i);
        }
        return sum;
    }

    public void calculateAlcoholScore(int unit) {
        this.alcoholUnit = unit;
        switch (unit) {
            case 0:
                this.alcoholScore = 6;
                break;
            case 1:
                this.alcoholScore = 5;
                break;
            case 2:
                this.alcoholScore = 3;
                break;
            default:
                if (unit > 2) {
                    this.alcoholScore = (unit - 2) * -2;
                }
        }
    }

    public void calculateBloodsScore(int systolic, int diastolic) {
        this.systolic = systolic;
        this.diastolic = diastolic;
        int systolicScore;
        int diastolicScore;

        if (systolic < 90) {
            systolicScore = 0;
        } else if (systolic >= 90 && systolic < 120) {
            systolicScore = 5;
        } else if (systolic >= 120 && systolic < 130) {
            systolicScore = 2;
        } else if (systolic >= 130 && systolic < 140) {
            systolicScore = 0;
        } else if (systolic >= 140 && systolic < 160) {
            systolicScore = -5;
        } else {
            systolicScore = -10;
        }

        if (diastolic < 60) {
            diastolicScore = 0;
        } else if (diastolic >= 60 && diastolic < 80) {
            diastolicScore = 5;
        } else if (diastolic >= 80 && diastolic < 85) {
            diastolicScore = 2;
        } else if (diastolic >= 85 && diastolic < 90) {
            diastolicScore = 0;
        } else if (diastolic >= 90 && diastolic < 100) {
            diastolicScore = -5;
        } else {
            diastolicScore = -10;
        }
        this.bloodsScore = (systolicScore + diastolicScore) / 2;
    }

    public void calculateDeStressScore(int level) {
        this.stressLevel = level;
        this.deStressScore = (level - 5) * (-1);
    }

    public void calculateDeToxScore(int cigarettes) {
        this.noCigarettes = cigarettes;
        this.deToxScore = cigarettes * (-2);
    }

    public void calculateDietScore(int dietLevel) {
        this.dietLevel = dietLevel;
        this.dietScore = (dietLevel - 5) * (-2);
    }

    public void calculateExerciseScore(int steps, int mins) {
        this.steps = steps;
        this.mins = mins;
        int stepsScore;
        int minsScore;

        if (steps < 2000 && mins < 5) {
            stepsScore = -5;
        } else if (steps >= 2000 && steps <= 4000) {
            stepsScore = (steps / 1000) - 2;
        } else if (steps > 4000 && steps <= 10000) {
            stepsScore = (steps / 1000) / 2;
        } else if (steps > 10000 && steps <= 1500) {
            stepsScore = (steps / 100) / 25 + 1;
        } else if (steps > 15000 && steps <= 20000) {
            stepsScore = 7;
        } else {
            stepsScore = 8;
        }

        if (mins < 5 && steps < 2000) {
            minsScore = -5;
        } else if (mins >= 5 && mins <= 30) {
            minsScore = (mins / 5) - 1;
        } else if (mins > 30 && mins <= 60) {
            minsScore = (mins / 10) + 2;
        } else {
            minsScore = 8;
        }

        this.exerciseScore = (stepsScore + minsScore) / 2;
    }

    public void calculateFivaADayScore(int fruitVeg) {
        this.noFruitVeg = fruitVeg;
        if (fruitVeg < 10) {
            this.fiveADayScore = fruitVeg;
        } else {
            this.fiveADayScore = 10;
        }
    }

    public void calculateHappyHoursScore(int hours) {
        this.noHappyHours = hours;
        this.happyHoursScore = hours;
    }

    public void calculateSleepScore(int hoursSleep) {
        this.noSleepHours = hoursSleep;
        if (hoursSleep == 8) {
            this.sleepScore = 5;
        } else if (hoursSleep == 7 || hoursSleep == 9) {
            this.sleepScore = 3;
        } else if (hoursSleep == 6 || hoursSleep == 10) {
            this.sleepScore = 1;
        } else if (hoursSleep == 5 || hoursSleep == 11) {
            this.sleepScore = -1;
        } else {
            this.sleepScore = -5;
        }
    }

    public void calculateSugarAndSaltScore(int sugar, int salt) {
        this.sugar = sugar;
        this.salt = salt;
        int sugarScore;
        int saltScore;
        if (sugar <= 25 && salt <= 5) {
            this.sugarAndSaltScore = 5;
        } else if (sugar > 25 && salt > 5) {
            sugarScore = ((sugar - 25) / 5) * (-2);
            saltScore = (salt - 5) * (-2);
            this.sugarAndSaltScore = sugarScore + saltScore;
        } else if (sugar <= 25) {
            sugarScore = 5;
            saltScore = (salt - 5) * (-2);
            this.sugarAndSaltScore = sugarScore + saltScore;
        } else if (salt <= 5) {
            saltScore = 5;
            sugarScore = ((sugar - 25) / 5) * (-2);
            this.sugarAndSaltScore = sugarScore + saltScore;
        }
    }

    public void calculateWaterScore(int glasses) {
        this.noGlasses = glasses;
        if (glasses < 6) {
            this.waterScore = glasses - 1;
        } else {
            this.waterScore = 5;
        }
    }

    public void calculateWeightScore(int bmi) {
        this.bmi = bmi;
        if (bmi <= 18) {
            this.weightScore = 2;
        } else if (bmi == 19) {
            this.weightScore = 4;
        } else if (bmi >= 20 && bmi <= 24) {
            this.weightScore = 5;
        } else if (bmi == 25) {
            this.weightScore = 2;
        } else if (bmi > 25 && bmi < 30) {
            this.weightScore = -1;
        } else if (bmi >= 30 && bmi <= 40) {
            this.weightScore = -5;
        } else {
            this.weightScore = -10;
        }
    }
}
