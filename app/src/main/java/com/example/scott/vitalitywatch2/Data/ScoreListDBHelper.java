package com.example.scott.vitalitywatch2.Data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import com.example.scott.vitalitywatch2.Data.ScoreListContract.*;

import java.io.File;

/**
 * Created by Scott on 03/12/2017.
 */

public class ScoreListDBHelper extends SQLiteOpenHelper {


    private static final String DATABASE_NAME = "scorelist.db";
    private static final int DATABASE_VERSION = 1;

    public ScoreListDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        final String SQL_CREATE_SCORELIST_TABLE = "CREATE OR UPDATE TABLE " +
                ScoreListEntry.TABLE_NAME + " (" +
                ScoreListEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                ScoreListEntry.COLUMN_DATE + " INTEGER NOT NULL UNIQUE, " +
                ScoreListEntry.COLUMN_SCORE + " INTEGER DEFAULT 0, " +

                ScoreListEntry.COLUMN_ALCOHOL_SCORE + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_ALCOHOL_DRINKS + " INTEGER DEFAULT 0, " +

                ScoreListEntry.COLUMN_BLOODS_SCORE + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_SYSTOLIC + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_DIASTOLIC + " INTEGER DEFAULT 0, " +

                ScoreListEntry.COLUMN_DESTRESS_SCORE + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_STRESS_LEVEL + " INTEGER DEFAULT 0, " +

                ScoreListEntry.COLUMN_DETOX_SCORE + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_CIGARETTES + " INTEGER DEFAULT 0, " +

                ScoreListEntry.COLUMN_DIET_SCORE + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_DIET_LEVEL + " INTEGER DEFAULT 0, " +

                ScoreListEntry.COLUMN_EXERCISE_SCORE + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_STEPS + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_MINS + " INTEGER DEFAULT 0, " +

                ScoreListEntry.COLUMN_FIVE_A_DAY_SCORE + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_NO_FRUIT_VEG + " INTEGER DEFAULT 0, " +

                ScoreListEntry.COLUMN_HAPPY_HOURS_SCORE + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_HAPPY_HOURS + " INTEGER DEFAULT 0, " +

                ScoreListEntry.COLUMN_SLEEP_SCORE + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_HOURS_SLEEP + " INTEGER DEFAULT 0, " +

                ScoreListEntry.COLUMN_SUGAR_SALT_SCORE + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_SUGAR + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_SALT + " INTEGER DEFAULT 0, " +

                ScoreListEntry.COLUMN_WATER_SCORE + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_GLASSES + " INTEGER DEFAULT 0, " +

                ScoreListEntry.COLUMN_WEIGHT_SCORE + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_BMI + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_WEIGHT + " INTEGER DEFAULT 0, " +
                ScoreListEntry.COLUMN_HEIGHT + " INTEGER DEFAULT 0); ";
        sqLiteDatabase.execSQL(SQL_CREATE_SCORELIST_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ScoreListEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
