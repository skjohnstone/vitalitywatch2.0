package com.example.scott.vitalitywatch2.Data;

import android.content.ContentValues;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sjohn on 06/12/2017.
 */

public class FakeData {

    public static void insertFakeData(SQLiteDatabase db, int date) {
        if (db != null) {

            List<ContentValues> list = new ArrayList<ContentValues>();


            ContentValues cv = new ContentValues();
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DATE, 0);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_ALCOHOL_DRINKS, 4);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_ALCOHOL_SCORE, -4);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_BLOODS_SCORE, 2);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SYSTOLIC, 75);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DIASTOLIC, 60);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_STRESS_LEVEL, 7);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DESTRESS_SCORE, -2);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_CIGARETTES, 0);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DETOX_SCORE, 0);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DIET_LEVEL, 5);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DIET_SCORE, 0);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_STEPS, 4000);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_MINS, 20);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_EXERCISE_SCORE, 2);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_NO_FRUIT_VEG, 5);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_FIVE_A_DAY_SCORE, 5);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_HAPPY_HOURS, 3);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_HAPPY_HOURS_SCORE, 3);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_HOURS_SLEEP, 6);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SLEEP_SCORE, 1);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SUGAR, 30);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SALT, 5);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SUGAR_SALT_SCORE, 3);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_GLASSES, 1);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_WATER_SCORE, 0);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_WEIGHT, 60);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_HEIGHT, 160);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_BMI, 22);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_WEIGHT_SCORE, 0);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SCORE, 15);

            list.add(cv);

            cv = new ContentValues();
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DATE, 1);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_ALCOHOL_DRINKS, 4);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_ALCOHOL_SCORE, -4);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_BLOODS_SCORE, 2);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SYSTOLIC, 75);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DIASTOLIC, 60);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_STRESS_LEVEL, 7);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DESTRESS_SCORE, -2);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_CIGARETTES, 0);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DETOX_SCORE, 0);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DIET_LEVEL, 5);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DIET_SCORE, 0);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_STEPS, 4000);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_MINS, 20);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_EXERCISE_SCORE, 2);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_NO_FRUIT_VEG, 5);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_FIVE_A_DAY_SCORE, 5);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_HAPPY_HOURS, 3);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_HAPPY_HOURS_SCORE, 3);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_HOURS_SLEEP, 6);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SLEEP_SCORE, 1);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SUGAR, 30);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SALT, 5);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SUGAR_SALT_SCORE, 3);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_GLASSES, 1);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_WATER_SCORE, 0);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_WEIGHT, 60);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_HEIGHT, 160);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_BMI, 22);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_WEIGHT_SCORE, 0);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SCORE, 15);

            list.add(cv);

            cv = new ContentValues();
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DATE, 2);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_ALCOHOL_DRINKS, 4);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_ALCOHOL_SCORE, -4);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_BLOODS_SCORE, 2);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SYSTOLIC, 75);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DIASTOLIC, 60);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_STRESS_LEVEL, 7);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DESTRESS_SCORE, -2);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_CIGARETTES, 0);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DETOX_SCORE, 0);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DIET_LEVEL, 5);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_DIET_SCORE, 0);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_STEPS, 4000);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_MINS, 20);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_EXERCISE_SCORE, 2);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_NO_FRUIT_VEG, 5);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_FIVE_A_DAY_SCORE, 5);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_HAPPY_HOURS, 3);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_HAPPY_HOURS_SCORE, 3);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_HOURS_SLEEP, 6);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SLEEP_SCORE, 1);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SUGAR, 30);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SALT, 5);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SUGAR_SALT_SCORE, 3);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_GLASSES, 1);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_WATER_SCORE, 0);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_WEIGHT, 60);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_HEIGHT, 160);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_BMI, 22);
            cv.put(ScoreListContract.ScoreListEntry.COLUMN_WEIGHT_SCORE, 0);

            cv.put(ScoreListContract.ScoreListEntry.COLUMN_SCORE, 15);

            list.add(cv);


            try {
                db.beginTransaction();
                long newRowID = db.insert(ScoreListContract.ScoreListEntry.TABLE_NAME, null, list.get(0));
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                db.endTransaction();
            }

        }
    }
}
