package com.example.scott.vitalitywatch2.Data;

/**
 * Created by sjohn on 03/12/2017.
 */

public class ScoresCalculator {

    public static int calculateAlcoholScore(int drinks) {
        int alcoholScore = 0;
        switch (drinks) {
            case 0:
                alcoholScore = 6;
                break;
            case 1:
                alcoholScore = 5;
                break;
            case 2:
                alcoholScore = 3;
                break;
            default:
                if (drinks > 2) {
                    alcoholScore = (drinks - 2) * -2;
                }
        }
        return alcoholScore;
    }

    public static int calculateBloodsScore(int systolic, int diastolic) {
        int systolicScore;
        int diastolicScore;
        int bloodsScore;

        if (systolic < 90) {
            systolicScore = 0;
        } else if (systolic >= 90 && systolic < 120) {
            systolicScore = 5;
        } else if (systolic >= 120 && systolic < 130) {
            systolicScore = 2;
        } else if (systolic >= 130 && systolic < 140) {
            systolicScore = 0;
        } else if (systolic >= 140 && systolic < 160) {
            systolicScore = -5;
        } else {
            systolicScore = -10;
        }

        if (diastolic < 60) {
            diastolicScore = 0;
        } else if (diastolic >= 60 && diastolic < 80) {
            diastolicScore = 5;
        } else if (diastolic >= 80 && diastolic < 85) {
            diastolicScore = 2;
        } else if (diastolic >= 85 && diastolic < 90) {
            diastolicScore = 0;
        } else if (diastolic >= 90 && diastolic < 100) {
            diastolicScore = -5;
        } else {
            diastolicScore = -10;
        }
        bloodsScore = (systolicScore + diastolicScore) / 2;
        return bloodsScore;
    }

    public static int calculateDeStressScore(int level) {
        return (level - 5) * (-1);
    }

    public static int calculateDeToxScore(int cigarettes) {
        return cigarettes * (-2);
    }

    public static int calculateDietScore(int dietLevel) {
        return (dietLevel - 5) * (-2);
    }

    public static int calculateExerciseScore(int steps, int mins) {
        int stepsScore;
        int minsScore;

        if (steps < 2000 && mins < 5) {
            stepsScore = -5;
        } else if (steps >= 2000 && steps <= 4000) {
            stepsScore = (steps / 1000) - 2;
        } else if (steps > 4000 && steps <= 10000) {
            stepsScore = (steps / 1000) / 2;
        } else if (steps > 10000 && steps <= 1500) {
            stepsScore = (steps / 100) / 25 + 1;
        } else if (steps > 15000 && steps <= 20000) {
            stepsScore = 7;
        } else {
            stepsScore = 8;
        }

        if (mins < 5 && steps < 2000) {
            minsScore = -5;
        } else if (mins >= 5 && mins <= 30) {
            minsScore = (mins / 5) - 1;
        } else if (mins > 30 && mins <= 60) {
            minsScore = (mins / 10) + 2;
        } else {
            minsScore = 8;
        }

        return (stepsScore + minsScore) / 2;
    }

    public static int calculateFivaADayScore(int fruitVeg) {
        int score;
        if (fruitVeg < 10) {
            score = fruitVeg;
        } else {
            score = 10;
        }
        return score;
    }

    public static int calculateHappyHoursScore(int hours) {
        return hours;
    }

    public static int calculateSleepScore(int hoursSleep) {
        int score;
        if (hoursSleep == 8) {
            score = 5;
        } else if (hoursSleep == 7 || hoursSleep == 9) {
            score = 3;
        } else if (hoursSleep == 6 || hoursSleep == 10) {
            score = 1;
        } else if (hoursSleep == 5 || hoursSleep == 11) {
            score = -1;
        } else {
            score = -5;
        }
        return score;
    }

    public static int calculateSugarAndSaltScore(int sugar, int salt) {
        int sugarScore;
        int saltScore;
        int score = 0;
        if (sugar <= 25 && salt <= 5) {
            score = 5;
        } else if (sugar > 25 && salt > 5) {
            sugarScore = ((sugar - 25) / 5) * (-2);
            saltScore = (salt - 5) * (-2);
            score = sugarScore + saltScore;
        } else if (sugar <= 25) {
            sugarScore = 5;
            saltScore = (salt - 5) * (-2);
            score = sugarScore + saltScore;
        } else if (salt <= 5) {
            saltScore = 5;
            sugarScore = ((sugar - 25) / 5) * (-2);
            score = sugarScore + saltScore;
        }
        int adaptedScore;
        if (score >= 0) {
            adaptedScore = score * 2;
        } else {
            adaptedScore = score;
        }
        if (adaptedScore < 10 ) {
            adaptedScore = -10;
        }
        return adaptedScore;
    }

    public static int calculateWaterScore(int glasses) {
        int score;
        if (glasses < 6) {
            score = glasses - 1;
        } else {
            score = 5;
        }

        int adaptedScore;

        if(score > 0) {

        }
        return score;
    }

    public static int calculateWeightScore(double bmi) {
        int score;
        if (bmi <= 18) {
            score = 2;
        } else if (bmi == 19) {
            score = 4;
        } else if (bmi >= 20 && bmi <= 24) {
            score = 5;
        } else if (bmi == 25) {
            score = 2;
        } else if (bmi > 25 && bmi < 30) {
            score = -1;
        } else if (bmi >= 30 && bmi <= 40) {
            score = -5;
        } else {
            score = -10;
        }
        return score;
    }

    public static int calculateBMI(int heightCM, int weight) {
        double bmi;
        double heightM = (double) heightCM / 100;
        double weightKG = (double) weight;
        bmi =  (weightKG/heightM)/heightM;
        int bmiInt = (int) Math.round(bmi);
        return bmiInt;
    }
}
