package com.example.scott.vitalitywatch2.WaitlistData;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.scott.vitalitywatch2.R;


public class GuestListAdapter {

    // Holds on to the cursor to display the waitlist
    private Cursor mCursor;

    /**
     * Constructor using the context and the db cursor
     * @param cursor the db cursor with waitlist data to display
     */
    public GuestListAdapter(Cursor cursor) {
        this.mCursor = cursor;
    }


    public void onBindViewHolder(int position) {
        // Move the mCursor to the position of the item to be displayed
        if (!mCursor.moveToPosition(position))
            return; // bail if returned null

        // Update the view holder with the information needed to display
        String name = mCursor.getString(mCursor.getColumnIndex(WaitlistContract.WaitlistEntry.COLUMN_GUEST_NAME));
        int partySize = mCursor.getInt(mCursor.getColumnIndex(WaitlistContract.WaitlistEntry.COLUMN_PARTY_SIZE));
        // COMPLETED (6) Retrieve the id from the cursor and
        long id = mCursor.getLong(mCursor.getColumnIndex(WaitlistContract.WaitlistEntry._ID));
    }
}