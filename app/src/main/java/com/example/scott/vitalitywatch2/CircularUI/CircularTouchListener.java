package com.example.scott.vitalitywatch2.CircularUI;

import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

/**
 * Created by sjohn on 05/12/2017.
 */

public class CircularTouchListener implements View.OnTouchListener {
    public interface CircularItemClickListener {/*
        void onItemClick(View view, int index);*/
    }

    private CircularItemClickListener itemClickListener;
    CircleButtonLayoutAdapter circleButtonLayoutAdapter;
    private float init_x = 0;
    private float init_y = 0;
    private float pre_x = 0;
    private float pre_y = 0;
    private float cur_x = 0;
    private float cur_y = 0;
    private float move_x = 0;
    private float move_y = 0;
    private float minClickDistance = 30.0f;
    private float minMoveDistance = 30.0f;
    private float mMovingSpeed = 2000.0f;  // default is 2000, larger > faster
    private boolean isCircularMoving = false; // ensure that item click only triggered when it's not moving


    public void setItemClickListener(CircularItemClickListener listener) {
        this.itemClickListener = listener;
    }

    public boolean onTouch(View v, MotionEvent event) {

        final CircleLayout circleLayout = (CircleLayout) v;
        circleButtonLayoutAdapter = circleLayout.getAdapter();

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                cur_x = event.getX();
                cur_y = event.getY();
                init_x = event.getX();
                init_y = event.getY();

            case MotionEvent.ACTION_MOVE:
                pre_x = cur_x;
                pre_y = cur_y;
                cur_x = event.getX();
                cur_y = event.getY();

                float diff_x = cur_x - pre_x;
                float diff_y = cur_y - pre_y;
                move_x = init_x - cur_x;
                move_y = init_y - cur_y;
                float moveDistance = (float) Math.sqrt(move_x * move_x + move_y * move_y);


                if (cur_y >= circleLayout.layoutCenter_y) diff_x = -diff_x;
                if (cur_x <= circleLayout.layoutCenter_x) diff_y = -diff_y;

                // should rotate the layout
                if (moveDistance > minMoveDistance) {
                    isCircularMoving = true;
                    CircleLayout.MoveAccumulator += (diff_x + diff_y) / mMovingSpeed;

                    // calculate new position around circle
                    for (int i = 0; i < circleButtonLayoutAdapter.getCount(); i++) {
                        final int idx = i;
                        final View buttonView = circleButtonLayoutAdapter.getViewAt(i);
                        buttonView.post(new Runnable() {
                            @Override
                            public void run() {
                                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
                                        buttonView.getLayoutParams();
                                params.setMargins(
                                        (int) (circleLayout.layoutCenter_x - (circleLayout.viewWidth / 2) +
                                                (circleLayout.radius * Math.cos(idx * circleLayout.getIntervalAngle() +
                                                        CircleLayout.MoveAccumulator * Math.PI * 2))),
                                        (int) (circleLayout.layoutCenter_y - (circleLayout.viewHeight / 2) +
                                                (circleLayout.radius * Math.sin(idx * circleLayout.getIntervalAngle() +
                                                        CircleLayout.MoveAccumulator * Math.PI * 2))),
                                        0,
                                        0);
                                buttonView.setLayoutParams(params);
                                buttonView.requestLayout();
                            }
                        });
                    }
                }

                return true;

            case MotionEvent.ACTION_UP:
                isCircularMoving = false; // reset moving state when event ACTION_UP
                return true;
        }
        return false;
    }

    
}
