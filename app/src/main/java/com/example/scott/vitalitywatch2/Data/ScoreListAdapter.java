package com.example.scott.vitalitywatch2.Data;

import android.content.Context;
import android.database.Cursor;

/**
 * Created by sjohn on 06/12/2017.
 */

public class ScoreListAdapter {
    private Cursor mCursor;
    public int[] scoresArray;
    private int totalScore;

    public ScoreListAdapter(Cursor cursor) {
        this.mCursor = cursor;
    }

    public int[] getScoresArray() {
        return scoresArray;
    }

    public int getTotalScore() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_SCORE));
    }


    public int getAlcoholDrinks() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_ALCOHOL_DRINKS));
    }
    public int getAlcoholScore() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_ALCOHOL_SCORE));
    }


    public int getDiastolicBlood() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_DIASTOLIC));
    }
    public int getSystolicBlood() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_SYSTOLIC));
    }
    public int getBloodScore() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_BLOODS_SCORE));
    }


    public int getStressLevel() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_STRESS_LEVEL));
    }
    public int getStressScore() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_DESTRESS_SCORE));
    }


    public int getNoCigarettes() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_CIGARETTES));
    }
    public int getDeToxScore() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_DETOX_SCORE));
    }


    public int getDietLevel() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_DIET_LEVEL));
    }
    public int getDietScore() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_DIET_SCORE));
    }


    public int getSteps() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_STEPS));
    }
    public int getMins() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_MINS));
    }
    public int getExerciseScore() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_EXERCISE_SCORE));
    }


    public int getFiveADayScore() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_FIVE_A_DAY_SCORE));
    }
    public int getFruitVeg() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_NO_FRUIT_VEG));
    }


    public int getHappyHours() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_HAPPY_HOURS));
    }
    public int getHappyHoursScore() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_HAPPY_HOURS_SCORE));
    }


    public int getSleepHours() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_HOURS_SLEEP));
    }
    public int getSleepScore() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_SLEEP_SCORE));
    }


    public int getSugar() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_SUGAR));
    }
    public int getSalt() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_SALT));
    }
    public int getSugarSaltScore() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_SUGAR_SALT_SCORE));
    }


    public int getGlassesWater() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_GLASSES));
    }
    public int getWaterScore() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_WATER_SCORE));
    }


    public int getWeightScore() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_WEIGHT_SCORE));
    }
    public int getWeight() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_WEIGHT));
    }
    public int getHeightCM() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_HEIGHT));
    }
    public int getBMI() {
        return mCursor.getInt(mCursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_BMI));
    }
}
