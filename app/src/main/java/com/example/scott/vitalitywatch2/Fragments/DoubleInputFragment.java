package com.example.scott.vitalitywatch2.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.example.scott.vitalitywatch2.Activities.MainActivity;

/**
 * Created by sjohn on 20/11/2017.
 */

public class DoubleInputFragment extends DialogFragment {
    private NumberPicker.OnValueChangeListener valueChangeListener;
    private String title;
    private String firstDescription;
    private String secondDescription;
    private int firstCurrent;
    private int secondCurrent;
    private int firstMin;
    private int secondMin;
    private int firstMax;
    private int secondMax;
    private int firstStepSize;
    private int secondStepSize;
    private int id;
    private NumberPicker firstNumberPicker;
    private NumberPicker secondNumberPicker;
    private LinearLayout mainParent;

    public void setFirstDetails (String description, int current, int max, int min, int stepSize) {
        this.firstCurrent = current;
        this.firstMax = max;
        this.firstMin = min;
        this.firstDescription = description;
        this.firstStepSize = stepSize;
    }

    public void setSecondDetails (String description, int current, int max, int min, int stepSize) {
        this.secondCurrent = current;
        this.secondMax = max;
        this.secondMin = min;
        this.secondDescription = description;
        this.secondStepSize = stepSize;
    }

    public void setDetails (String title, int id) {
        this.title = title;
        this.id = id;
    }

    private void setLayout() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        //Initialise first number picker
        firstNumberPicker = new NumberPicker(getActivity());
        if (firstStepSize != 1) {
            firstNumberPicker.setDisplayedValues(null);
            String[] valueSet = new String[(1 + (firstMax - firstMin) / firstStepSize)];
            for (int i = 0; i < valueSet.length; i++) {
                valueSet[i] = String.valueOf(firstMin + (i * firstStepSize));
                System.out.println(valueSet[i]);
            }
            firstNumberPicker.setMinValue(0);
            firstNumberPicker.setMaxValue(valueSet.length - 1);
            firstNumberPicker.setDisplayedValues(valueSet);
        } else {
            firstNumberPicker.setMaxValue(firstMax);
            firstNumberPicker.setMinValue(firstMin);
            firstNumberPicker.setValue(firstCurrent);

        }

        //Initialise second number picker
        secondNumberPicker = new NumberPicker(getActivity());
        if (secondStepSize != 1) {
            secondNumberPicker.setValue((secondCurrent / secondStepSize) - secondMin);
            secondNumberPicker.setDisplayedValues(null);
            String[] valueSet = new String[1 + (secondMax - secondMin) / secondStepSize];
            for (int i = 0; i < valueSet.length; i++) {
                valueSet[i] = String.valueOf(secondMin + (i * secondStepSize));
            }
            secondNumberPicker.setMaxValue(valueSet.length - 1);
            secondNumberPicker.setMinValue(0);
            secondNumberPicker.setDisplayedValues(valueSet);
        } else {
            secondNumberPicker.setValue(secondCurrent);
            secondNumberPicker.setMaxValue(secondMax);
            secondNumberPicker.setMinValue(secondMin);

        }

        //Initialise first number picker layout
        LinearLayout firstVertical = new LinearLayout((getActivity()));
        firstVertical.setLayoutParams(layoutParams);
        firstVertical.setWeightSum(1);
        firstVertical.setOrientation(LinearLayout.VERTICAL);
        TextView firstTextView = new TextView(getActivity());
        firstTextView.setText(firstDescription);
        firstVertical.addView(firstTextView);
        firstVertical.addView(firstNumberPicker);

        //Initialise second number picker layout
        LinearLayout secondVertical = new LinearLayout(getActivity());
        secondVertical.setLayoutParams(layoutParams);
        secondVertical.setPadding(2,2,2,2);
        secondVertical.setWeightSum(1);
        secondVertical.setOrientation(LinearLayout.VERTICAL);
        TextView secondTextView = new TextView(getActivity());
        secondTextView.setText(secondDescription);
        secondVertical.addView(secondTextView);
        secondVertical.addView(secondNumberPicker);

        //Initialise main parent view for number pickers
        mainParent = new LinearLayout(getActivity());
        mainParent.setLayoutParams(layoutParams);
        mainParent.setGravity(Gravity.CENTER);
        mainParent.setOrientation(LinearLayout.HORIZONTAL);
        mainParent.addView(firstVertical);
        mainParent.addView(secondVertical);
    }

    public Dialog onCreateDialog (Bundle savedInstanceState) {
        setLayout();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title)
                .setView(mainParent)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity callingActivity = (MainActivity) getActivity();
                        callingActivity.onDialogOK(firstNumberPicker, secondNumberPicker, id);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        return builder.create();
    }
}
