package com.example.scott.vitalitywatch2.Fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import com.example.scott.vitalitywatch2.Activities.MainActivity;

/**
 * Created by sjohn on 03/11/2017.
 */

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog (Bundle savedInstanceState) {
        int day = MainActivity.displayedDay;
        int month = MainActivity.displayedMonth - 1;
        int year = MainActivity.displayedYear;
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, day, month, year);
        datePickerDialog.updateDate(year, month, day);
        return datePickerDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        MainActivity callingActivity = (MainActivity) getActivity();
        callingActivity.onDateSet(dayOfMonth, month, year);
    }
}
