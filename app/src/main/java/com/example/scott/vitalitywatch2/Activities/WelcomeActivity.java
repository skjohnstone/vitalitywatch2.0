package com.example.scott.vitalitywatch2.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.scott.vitalitywatch2.R;
import com.example.scott.vitalitywatch2.Data.ScoresCalculator;

/**
 * Created by Scott on 03/12/2017.
 */

public class WelcomeActivity extends AppCompatActivity {
    private NumberPicker heightNumberPicker;
    private NumberPicker weightNumberPicker;
    private EditText nameEditText;
    private ToggleButton heightToggle;
    private ToggleButton weightToggle;
    private Boolean heightMetric;
    private Boolean weightMetric;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        heightNumberPicker = findViewById(R.id.heightNumberPicker);
        weightNumberPicker = findViewById(R.id.weightNumberPicker);
        nameEditText = findViewById(R.id.nameEditText);
        heightToggle = findViewById(R.id.heightToggle);
        weightToggle = findViewById(R.id.weightToggle);
        nameEditText = findViewById(R.id.nameEditText);
        heightMetric = true;
        weightMetric = true;
        initNumberPickers();
        heightToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int newValue;
                double value;
                int constantValue = weightNumberPicker.getValue();
                if (isChecked) { //Convert from imperial to metric
                    heightMetric = true;
                    value = (double) heightNumberPicker.getValue();
                    value = value * 2.54;
                    newValue = (int) Math.round(value);
                } else { //Convert from  metric to imperial
                    heightMetric = false;
                    value = (double) heightNumberPicker.getValue();
                    value = value / 2.54;
                    newValue = (int) Math.round(value);
                }
                initNumberPickers();
                heightNumberPicker.setValue(newValue);
                weightNumberPicker.setValue(constantValue);
            }
        });
        weightToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Double value;
                int constantValue = heightNumberPicker.getValue();
                int newValue;
                if (isChecked) { //Convert from imperial to metric
                    weightMetric = true;
                    value = (double) weightNumberPicker.getValue();
                    value = value / 2.20462;
                    newValue = (int) Math.round(value);
                } else { //Convert from metric to imperial
                    weightMetric = false;
                    value = (double) weightNumberPicker.getValue();
                    value = value * 2.20462;
                    newValue = (int) Math.round(value);
                }
                initNumberPickers();
                weightNumberPicker.setValue(newValue);
                heightNumberPicker.setValue(constantValue);
            }
        });

    }

    public void submitButtonPressed(View view) {
        double weightDouble = (double) (weightNumberPicker.getValue());
        double heightDouble = (double) (heightNumberPicker.getValue());
        if (!heightMetric) {
            heightDouble = heightDouble * 2.54;
        }
        if (!weightMetric) {
            weightDouble = weightDouble /2.20462 ;
        }
        Integer heightCM = (int) Math.round(heightDouble);
        Integer weightG = (int) Math.round(weightDouble);
        double bmi = ScoresCalculator.calculateBMI(heightCM, weightG);
        String text = "Height: " + heightCM + "cm\nWeight: " + weightG + "kg\nBMI: " + bmi;
        Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
        toast.show();
        String userName = nameEditText.getText().toString();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("set_user_name", userName);
        editor.putString("set_user_height", heightCM.toString());
        editor.putString("set_user_weight", weightG.toString());
        editor.putBoolean("set_first_time_use", false);
        editor.apply();
        Intent startMainActivity = new Intent(this, MainActivity.class);
        startActivity(startMainActivity);
    }

    public void initNumberPickers() {

        //Height Picker
        if (this.heightMetric) {
            String metricHeight[] = new String[275];
            for(int i = 0; i < metricHeight.length; i++) {
                metricHeight[i] = (i/100) + "m " + (i%100) + "cm";
            }
            heightNumberPicker.setDisplayedValues(null);
            heightNumberPicker.setMinValue(0);
            heightNumberPicker.setMaxValue(metricHeight.length-1);
            heightNumberPicker.setDisplayedValues(metricHeight);
            heightNumberPicker.setValue(165);
        } else {
            String imperialHeight[] = new String[108];
            for(int i = 0; i < imperialHeight.length; i++) {
                imperialHeight[i] = (i/12) + "ft " + (i%12) + "in";
            }
            heightNumberPicker.setDisplayedValues(null);
            heightNumberPicker.setMinValue(0);
            heightNumberPicker.setMaxValue(imperialHeight.length-1);
            heightNumberPicker.setDisplayedValues(imperialHeight);
            heightNumberPicker.setValue(67);
    }

        //Weight Picker
        if (this.weightMetric) {
            String metricWeight[] = new String[200];
            for (int i = 0; i < metricWeight.length; i++) {
                metricWeight[i] = i + "kg";
            }
            weightNumberPicker.setDisplayedValues(null);
            weightNumberPicker.setMinValue(0);
            weightNumberPicker.setMaxValue(metricWeight.length-1);
            weightNumberPicker.setDisplayedValues(metricWeight);
            weightNumberPicker.setValue(80);
        } else {
            String imperialWeight[] = new String[441];
            for (int i = 0; i < imperialWeight.length; i++) {
                imperialWeight[i] = (i/14) + "st " + (i%14) + "lbs";
            }
            weightNumberPicker.setDisplayedValues(null);
            weightNumberPicker.setMinValue(0);
            weightNumberPicker.setMaxValue(imperialWeight.length-1);
            weightNumberPicker.setDisplayedValues(imperialWeight);
            weightNumberPicker.setValue(154);
        }

    }

}
