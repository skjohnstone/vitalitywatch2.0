package com.example.scott.vitalitywatch2.Activities;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.scott.vitalitywatch2.CircularUI.CircleButtonLayoutAdapter;
import com.example.scott.vitalitywatch2.CircularUI.CircleLayout;
import com.example.scott.vitalitywatch2.Data.FakeData;
import com.example.scott.vitalitywatch2.Data.ScoreListAdapter;
import com.example.scott.vitalitywatch2.Data.ScoreListContract;
import com.example.scott.vitalitywatch2.Data.ScoreListDBHelper;
import com.example.scott.vitalitywatch2.Fragments.DatePickerFragment;
import com.example.scott.vitalitywatch2.Fragments.DoubleInputFragment;
import com.example.scott.vitalitywatch2.Fragments.SingleInputFragment;
import com.example.scott.vitalitywatch2.R;
import com.example.scott.vitalitywatch2.SerializableMethod.ScoreList;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private CircleButtonLayoutAdapter circleButtonLayoutAdapter;
    private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
    private SimpleDateFormat intDateFormatter = new SimpleDateFormat("ddMMyyyy");
    private SimpleDateFormat dayFormatter = new SimpleDateFormat("dd");
    private SimpleDateFormat monthFormatter= new SimpleDateFormat ("MM");
    private SimpleDateFormat yearFormatter = new SimpleDateFormat("yyyy");
    public static int displayedDay;
    public static int displayedMonth;
    public static int displayedYear;
    private Date currDate;
    private Integer dateInt;
    private SQLiteDatabase mDb;
    private ScoreListAdapter mAdapter;
    private ScoreList scoreList;
    private String filePath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        if(settings.getBoolean("set_first_time_use", true)) {
            Intent startWelcomeActivity = new Intent(this, WelcomeActivity.class);
            startActivity(startWelcomeActivity);
        } else {

            //Set circle layout
            setCircleLayout();

            //SQLite
            /*ScoreListDBHelper dbHelper = new ScoreListDBHelper(this);
            mDb = dbHelper.getWritableDatabase();
            FakeData.insertFakeData(mDb, dateInt);
            addNewDate();
            Cursor cursor = getAllForCurrDate();
            mAdapter = new ScoreListAdapter(cursor);
            Log.d("Foo", "Cursor is: " + cursor);*/


            // Serializable
            //Set file path to be used to read/write
            filePath = getApplicationContext().getFilesDir().getPath().toString() + "scores.ser";
            try {
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filePath));
                this.scoreList = (ScoreList) ois.readObject();
                ois.close();
                System.out.println("file found, opened current scorelist");
            } catch (FileNotFoundException error) {
                this.scoreList = new ScoreList(this.currDate);
                System.out.println("File not found, created new score list");
            } catch (Exception error) {
                error.printStackTrace();

                System.out.println("other error");
            }
            //Set all points text fields

            scoreList.scoreListDateChanged(currDate);
            updateFields();
        }

    }

    public void addScoresButtonPressed(View view) {
    }

    public Cursor getAllForCurrDate() {
        String selection = ScoreListContract.ScoreListEntry.COLUMN_DATE + " = ?";
        String[] selectionArgs = { "0"};
        return mDb.query(
                ScoreListContract.ScoreListEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );
    }

    public void updateScoreFields(Cursor cursor) {
        int[] scoresArray = new int[12];
        if(cursor.moveToPosition(0)) {
            scoresArray[0] = cursor.getInt(cursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_ALCOHOL_SCORE));
            scoresArray[1] = cursor.getInt(cursor.getColumnIndex(ScoreListContract.ScoreListEntry.COLUMN_BLOODS_SCORE));
            scoresArray[2] = mAdapter.getStressScore();
            scoresArray[3] = mAdapter.getDeToxScore();
            scoresArray[4] = mAdapter.getDietScore();
            scoresArray[5] = mAdapter.getExerciseScore();
            scoresArray[6] = mAdapter.getFiveADayScore();
            scoresArray[7] = mAdapter.getHappyHoursScore();
            scoresArray[8] = mAdapter.getSleepScore();
            scoresArray[9] = mAdapter.getSugarSaltScore();
            scoresArray[10] = mAdapter.getWaterScore();
            scoresArray[11] = mAdapter.getWeightScore();
        } else {
            for(int i = 0; i < scoresArray.length; i++) {
                scoresArray[i] = i + 1;
            }
        }
        System.out.println(scoresArray.length);
        for (int i =0; i < scoresArray.length; i++) {
            final View view = circleButtonLayoutAdapter.getViewAt(i);
            TextView pointsText = view.findViewById(R.id.button_point_score);
            pointsText.setText("" + scoresArray[i]);


        }
        View backgroundView = circleButtonLayoutAdapter.getBackground();
        TextView totalScoreText = backgroundView.findViewById(R.id.todays_score);
        totalScoreText.setText("" + mAdapter.getTotalScore());
    }

    public void addNewDate() {
        ContentValues cv = new ContentValues();
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_DATE, dateInt);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_ALCOHOL_DRINKS, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_ALCOHOL_SCORE, 0);

        cv.put(ScoreListContract.ScoreListEntry.COLUMN_BLOODS_SCORE, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_SYSTOLIC, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_DIASTOLIC, 0);

        cv.put(ScoreListContract.ScoreListEntry.COLUMN_STRESS_LEVEL, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_DESTRESS_SCORE, 0);

        cv.put(ScoreListContract.ScoreListEntry.COLUMN_CIGARETTES, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_DETOX_SCORE, 0);

        cv.put(ScoreListContract.ScoreListEntry.COLUMN_DIET_LEVEL, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_DIET_SCORE, 0);

        cv.put(ScoreListContract.ScoreListEntry.COLUMN_STEPS, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_MINS, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_EXERCISE_SCORE, 0);

        cv.put(ScoreListContract.ScoreListEntry.COLUMN_NO_FRUIT_VEG, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_FIVE_A_DAY_SCORE, 0);

        cv.put(ScoreListContract.ScoreListEntry.COLUMN_HAPPY_HOURS, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_HAPPY_HOURS_SCORE, 0);

        cv.put(ScoreListContract.ScoreListEntry.COLUMN_HOURS_SLEEP, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_SLEEP_SCORE, 0);

        cv.put(ScoreListContract.ScoreListEntry.COLUMN_SUGAR, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_SALT, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_SUGAR_SALT_SCORE, 0);

        cv.put(ScoreListContract.ScoreListEntry.COLUMN_GLASSES, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_WATER_SCORE, 0);

        cv.put(ScoreListContract.ScoreListEntry.COLUMN_WEIGHT, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_HEIGHT, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_BMI, 0);
        cv.put(ScoreListContract.ScoreListEntry.COLUMN_WEIGHT_SCORE, 0);

        cv.put(ScoreListContract.ScoreListEntry.COLUMN_SCORE, 0);
    }

    public void moveDbToPublic() {

        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        FileChannel source=null;
        FileChannel destination=null;
        String currentDBPath = "/data/data/com.example.scott.vitalitywatch2/databases/" + ScoreListContract.ScoreListEntry.TABLE_NAME+".db";
        String backupDBPath = ScoreListContract.ScoreListEntry.TABLE_NAME;
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(sd, backupDBPath);
        try {
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
            Toast.makeText(this, "DB Exported!", Toast.LENGTH_LONG).show();
        } catch(IOException e) {
            e.printStackTrace();
        }

        try {
            currentDBPath = mDb.getPath().toString();
            currentDB = new File(data, currentDBPath);
            source = new FileInputStream(currentDB).getChannel();
            destination = new FileOutputStream(backupDB).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
            Toast.makeText(this, "DB Exported!", Toast.LENGTH_LONG).show();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    //Sets date from users input values to date picker.
    public void onDateSet(int selectedDay, int selectedMonth, int selectedYear){
        String dateString = "" + selectedDay + "/" + selectedMonth + "/" + selectedYear;
        try {
            currDate = dateFormatter.parse(dateString);
            TextView dateView = findViewById(R.id.date_text_view); // add (TextView) if breaks
            dateView.setText(dateFormatter.format(currDate));
            displayedDay = selectedDay;
            displayedMonth = selectedMonth;
            displayedYear = selectedYear;
            dateInt = Integer.parseInt(intDateFormatter.format(currDate).toString());

            //Serializable
            scoreList.scoreListDateChanged(currDate);
            updateFields();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    //updateFields for serializable
    public void updateFields() {
        ArrayList<Integer> scoresArray = scoreList.getScore().getAllScores();
        for(int i = 0; i<circleButtonLayoutAdapter.getCount(); i++) {
            final View view = circleButtonLayoutAdapter.getViewAt(i);
            TextView pointsText = view.findViewById(R.id.button_point_score);
            pointsText.setText("" + scoresArray.get(i));
        }

        View backgroundView = circleButtonLayoutAdapter.getBackground();
        TextView totalScoreText = backgroundView.findViewById(R.id.todays_score);
        String totalScoreString = Integer.toString(scoreList.getScore().getTotalScore());
        totalScoreText.setText(totalScoreString);

        //Fill graph
        GraphView graph = (GraphView) findViewById(R.id.graphOne);
        graph.removeAllSeries();
        int daysShown = 7;
        DataPoint[] dp = new DataPoint[daysShown];
        Date d;
        Calendar c = Calendar.getInstance();
        int score[] = new int[daysShown];
        int j = score.length - 1;
        for(int i = score.length - 1; i >= 0; i--) {
            d = currDate;
            c.setTime(d);
            c.add(Calendar.DATE, -j);
            d = c.getTime();
            score[i] = scoreList.getScoreFromDate(d);
            dp[i] = new DataPoint(i, score[i]);
            j--;
        }
        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(dp);
        graph.getGridLabelRenderer().setNumHorizontalLabels(daysShown);
        graph.getGridLabelRenderer().setHorizontalAxisTitle("Day's Ago");
        graph.getGridLabelRenderer().setVerticalAxisTitle("Score");
        graph.addSeries(series);
    }

    //onDialogOK for serializable
    public void onDialogOK (NumberPicker numberPickerOne, NumberPicker numberPickerTwo, int id) {
        int pickerOneIndex;
        int pickerTwoIndex;
        int pickerOneValue;
        int pickerTwoValue;
        String[] pickerOneArray;
        String[] pickerTwoArray;
        switch (id) {
            //Alcohol function
            case 0:
                scoreList.getScore().calculateAlcoholScore(numberPickerOne.getValue());
                break;

            //Bloods function
            case 1:
                pickerOneIndex = numberPickerOne.getValue();
                pickerOneArray = numberPickerOne.getDisplayedValues();
                pickerOneValue = Integer.parseInt(pickerOneArray[pickerOneIndex]);

                pickerTwoIndex = numberPickerTwo.getValue();
                pickerTwoArray = numberPickerTwo.getDisplayedValues();
                pickerTwoValue = Integer.parseInt(pickerTwoArray[pickerTwoIndex]);

                scoreList.getScore().calculateBloodsScore(pickerOneValue, pickerTwoValue);
                break;

            //De-Stress function
            case 2:
                scoreList.getScore().calculateDeStressScore(numberPickerOne.getValue());
                break;

            //De-Tox function
            case 3:
                scoreList.getScore().calculateDeToxScore(numberPickerOne.getValue());
                break;

            //Diet function
            case 4:
                scoreList.getScore().calculateDietScore(numberPickerOne.getValue());
                break;

            //Exercise function
            case 5:
                pickerOneIndex = numberPickerOne.getValue();
                pickerOneArray = numberPickerOne.getDisplayedValues();
                pickerOneValue = Integer.parseInt(pickerOneArray[pickerOneIndex]);

                pickerTwoIndex = numberPickerTwo.getValue();
                pickerTwoArray = numberPickerTwo.getDisplayedValues();
                pickerTwoValue = Integer.parseInt(pickerTwoArray[pickerTwoIndex]);
                scoreList.getScore().calculateExerciseScore(pickerOneValue, pickerTwoValue);
                break;

            //5 a Day function
            case 6:
                scoreList.getScore().calculateFivaADayScore(numberPickerOne.getValue());
                break;

            //Happy Hours function
            case 7:
                scoreList.getScore().calculateHappyHoursScore(numberPickerOne.getValue());
                break;

            //Sleep function
            case 8:
                scoreList.getScore().calculateSleepScore(numberPickerOne.getValue());
                break;

            //Sugar and Salt function
            case 9:
                pickerOneValue = numberPickerOne.getValue();
                pickerTwoValue = numberPickerTwo.getValue();
                scoreList.getScore().calculateSugarAndSaltScore(pickerOneValue, pickerTwoValue);
                break;

            //Water function
            case 10:
                scoreList.getScore().calculateWaterScore(numberPickerOne.getValue());
                break;

            //Weight function
            case 11:
                scoreList.getScore().calculateWeightScore(numberPickerOne.getValue());
                break;

            default:
                Toast.makeText(this,"Error on calculate", Toast.LENGTH_SHORT).show();
                break;
        }
        updateFields();
        save();
    }

    //onClick for serializable
    @Override
    public void onClick(View view) {
        FragmentManager fragMan = getFragmentManager();
        SingleInputFragment singleInput = new SingleInputFragment();
        DoubleInputFragment doubleInput = new DoubleInputFragment();
        int currentValue;
        switch (view.getId()) {
            //Change date button
            case R.id.date_button:
                DialogFragment newFragment;
                newFragment = new DatePickerFragment();
                newFragment.show(fragMan, "datePicker");
                break;
            //Alcohol button
            case 0:
                currentValue = scoreList.getScore().getAlcoholUnit();
                singleInput.setDetails("Alcohol", "Enter number of units: ", currentValue, 0, 20, 1, 0);
                singleInput.show(fragMan, "alcoholInput");
                break;
            //Bloods button
            case 1:
                int systolic = scoreList.getScore().getSystolic();
                int diastolic = scoreList.getScore().getDiastolic();
                doubleInput.setDetails("Bloods", view.getId());
                doubleInput.setFirstDetails("Systolic(mm Hg): ", systolic, 200, 60, 5);
                doubleInput.setSecondDetails("Diastolic(mm Hg): ", diastolic, 120, 40, 2);
                doubleInput.show(fragMan, "bloodsInput");
                break;
            //De-Stress button
            case 2:
                currentValue = scoreList.getScore().getStressLevel();
                singleInput.setDetails("De-Stress", "Enter current stress level \n0: no stress \n10:very stressed", currentValue, 0, 10, 1, 2);
                singleInput.show(fragMan, "deStressInput");
                break;
            //De-Tox button
            case 3:
                currentValue = scoreList.getScore().getNoCigarettes();
                singleInput.setDetails("De=Tox", "Enter no of cigarettes today:", currentValue, 0, 30, 1, 3);
                singleInput.show(fragMan, "deToxInput");
                break;
            //Diet button
            case 4:
                currentValue = scoreList.getScore().getDietLevel();
                singleInput.setDetails("Diet", "10: not kept to diet \n0: kept to diet perfect", currentValue, 0, 10, 1, 4);
                singleInput.show(fragMan, "dietInput");
                break;
            //Exercise button
            case 5:
                int steps = scoreList.getScore().getSteps();
                int mins = scoreList.getScore().getMins();
                doubleInput.setDetails("Exercise", view.getId());
                doubleInput.setFirstDetails("Enter Steps: ", steps, 25000, 0, 200);
                doubleInput.setSecondDetails("Enter mins exercise: ", mins, 80, 0, 5);
                doubleInput.show(fragMan, "exerciseInput");
                break;
            //5 a Day button
            case 6:
                currentValue = scoreList.getScore().getNoFruitVeg();
                singleInput.setDetails("5 a Day", "Enter no of fruit/veg today:", currentValue, 0, 10, 1, 6);
                singleInput.show(fragMan, "fiveADayInput");
                break;
            //Happy Hours button
            case 7:
                currentValue = scoreList.getScore().getNoHappyHours();
                singleInput.setDetails("Happy Hours", "Enter no of fun hours today:", currentValue, 0, 10, 1, 7);
                singleInput.show(fragMan, "happyHoursInput");
                break;
            //Sleep button
            case 8:
                currentValue = scoreList.getScore().getNoSleepHours();
                singleInput.setDetails("Sleep", "Enter no of hours sleep:", currentValue, 0, 15, 1, 8);
                singleInput.show(fragMan, "fiveADayInput");
                break;
            //Sugar and Salt button
            case 9:
                int sugar = scoreList.getScore().getSugar();
                int salt = scoreList.getScore().getSalt();
                doubleInput.setDetails("Sugar and Salt", view.getId());
                doubleInput.setFirstDetails("Enter grams of sugar: ", sugar, 100, 0, 1);
                doubleInput.setSecondDetails("Enter grams of salt: ", salt, 50, 0, 1);
                doubleInput.show(fragMan, "sugarSaltInput");
                break;
            //Water button
            case 10:
                currentValue = scoreList.getScore().getNoGlasses();
                singleInput.setDetails("Water", "Enter no of glasses of water today:", currentValue, 0, 10, 1, 10);
                singleInput.show(fragMan, "waterInput");
                break;
            //Weight button
            case 11:
                currentValue = scoreList.getScore().getBmi();
                singleInput.setDetails("Weight", "Enter BMI:", currentValue, 0, 50, 1, 11);
                singleInput.show(fragMan, "weightInput");
                break;

            default:
                Toast.makeText(this,"Error button not found", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    //Saves objects for serializable
    public void save() {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath));
            oos.writeObject(this.scoreList);
            oos.close();
        } catch (Exception error) {
            System.out.println("Error on saving");
            error.printStackTrace();
        }
    }

    //onClick for SQLite
    /*@Override
    public void onClick(View view) {
        FragmentManager fragMan = getFragmentManager();
        SingleInputFragment singleInput = new SingleInputFragment();
        DoubleInputFragment doubleInput = new DoubleInputFragment();
        int currentValue;
        switch (view.getId()) {
            //Change date button
            case R.id.date_button:
                DialogFragment newFragment;
                newFragment = new DatePickerFragment();
                newFragment.show(fragMan, "datePicker");
                break;
            //Alcohol button
            case 0:
                currentValue = 0;
                singleInput.setDetails("Alcohol", "Enter number of units: ", currentValue, 0, 20, 1, 0);
                singleInput.show(fragMan, "alcoholInput");
                break;
            //Bloods button
            case 1:
                int systolic = 0;
                int diastolic = 0;
                doubleInput.setDetails("Bloods", view.getId());
                doubleInput.setFirstDetails("Systolic(mm Hg): ", systolic, 200, 60, 5);
                doubleInput.setSecondDetails("Diastolic(mm Hg): ", diastolic, 120, 40, 2);
                doubleInput.show(fragMan, "bloodsInput");
                break;
            //De-Stress button
            case 2:
                currentValue = 0;
                singleInput.setDetails("De-Stress", "Enter current stress level \n0: no stress \n10:very stressed", currentValue, 0, 10, 1, 2);
                singleInput.show(fragMan, "deStressInput");
                break;
            //De-Tox button
            case 3:
                currentValue = 0;
                singleInput.setDetails("De=Tox", "Enter no of cigarettes today:", currentValue, 0, 30, 1, 3);
                singleInput.show(fragMan, "deToxInput");
                break;
            //Diet button
            case 4:
                currentValue = 0;
                singleInput.setDetails("Diet", "10: not kept to diet \n0: kept to diet perfect", currentValue, 0, 10, 1, 4);
                singleInput.show(fragMan, "dietInput");
                break;
            //Exercise button
            case 5:
                int steps = 0;
                int mins = 0;
                doubleInput.setDetails("Exercise", view.getId());
                doubleInput.setFirstDetails("Enter Steps: ", steps, 25000, 0, 200);
                doubleInput.setSecondDetails("Enter mins exercise: ", mins, 80, 0, 5);
                doubleInput.show(fragMan, "exerciseInput");
                break;
            //5 a Day button
            case 6:
                currentValue = 0;
                singleInput.setDetails("5 a Day", "Enter no of fruit/veg today:", currentValue, 0, 10, 1, 6);
                singleInput.show(fragMan, "fiveADayInput");
                break;
            //Happy Hours button
            case 7:
                currentValue = 0;
                singleInput.setDetails("Happy Hours", "Enter no of fun hours today:", currentValue, 0, 10, 1, 7);
                singleInput.show(fragMan, "happyHoursInput");
                break;
            //Sleep button
            case 8:
                currentValue = 0;
                singleInput.setDetails("Sleep", "Enter no of hours sleep:", currentValue, 0, 15, 1, 8);
                singleInput.show(fragMan, "fiveADayInput");
                break;
            //Sugar and Salt button
            case 9:
                int sugar = 0;
                int salt = 0;
                doubleInput.setDetails("Sugar and Salt", view.getId());
                doubleInput.setFirstDetails("Enter grams of sugar: ", sugar, 100, 0, 1);
                doubleInput.setSecondDetails("Enter grams of salt: ", salt, 50, 0, 1);
                doubleInput.show(fragMan, "sugarSaltInput");
                break;
            //Water button
            case 10:
                currentValue = 0;
                singleInput.setDetails("Water", "Enter no of glasses of water today:", currentValue, 0, 10, 1, 10);
                singleInput.show(fragMan, "waterInput");
                break;
            //Weight button
            case 11:
                currentValue = 0;
                singleInput.setDetails("Weight", "Enter BMI:", currentValue, 0, 50, 1, 11);
                singleInput.show(fragMan, "weightInput");
                break;

            default:
                Toast.makeText(this,"Error button not found", Toast.LENGTH_SHORT).show();
                break;
        }
    }*/

    //onDialogOK for SQLite
    /*public void onDialogOK (NumberPicker numberPickerOne, NumberPicker numberPickerTwo, int id) {
        int pickerOneIndex;
        int pickerTwoIndex;
        int pickerOneValue;
        int pickerTwoValue;
        int singleValue = numberPickerOne.getValue();
        String[] pickerOneArray;
        String[] pickerTwoArray;
        switch (id) {
            //Alcohol function
            case 0:
                break;

            //Bloods function
            case 1:
                pickerOneIndex = numberPickerOne.getValue();
                pickerOneArray = numberPickerOne.getDisplayedValues();
                pickerOneValue = Integer.parseInt(pickerOneArray[pickerOneIndex]);

                pickerTwoIndex = numberPickerTwo.getValue();
                pickerTwoArray = numberPickerTwo.getDisplayedValues();
                pickerTwoValue = Integer.parseInt(pickerTwoArray[pickerTwoIndex]);
                break;

            //De-Stress function
            case 2:
                break;

            //De-Tox function
            case 3:
                break;

            //Diet function
            case 4:
                break;

            //Exercise function
            case 5:
                pickerOneIndex = numberPickerOne.getValue();
                pickerOneArray = numberPickerOne.getDisplayedValues();
                pickerOneValue = Integer.parseInt(pickerOneArray[pickerOneIndex]);

                pickerTwoIndex = numberPickerTwo.getValue();
                pickerTwoArray = numberPickerTwo.getDisplayedValues();
                pickerTwoValue = Integer.parseInt(pickerTwoArray[pickerTwoIndex]);
                break;

            //5 a Day function
            case 6:
                break;

            //Happy Hours function
            case 7:
                break;

            //Sleep function
            case 8:
                break;

            //Sugar and Salt function
            case 9:
                pickerOneValue = numberPickerOne.getValue();
                pickerTwoValue = numberPickerTwo.getValue();
                break;

            //Water function
            case 10:
                break;

            //Weight function
            case 11:
                break;

            default:
                Toast.makeText(this,"Error on calculate", Toast.LENGTH_SHORT).show();
                break;
        }
    }*/

    //Set circle layout
    public void setCircleLayout() {
        final CircleLayout circleLayout = findViewById(R.id.circle_layout);
        circleButtonLayoutAdapter = new CircleButtonLayoutAdapter(getLayoutInflater());
        circleLayout.setAdapter(circleButtonLayoutAdapter);
        final View backgroundView = circleButtonLayoutAdapter.getBackground();
        Button dateButton = backgroundView.findViewById(R.id.date_button);
        dateButton.setOnClickListener(this);
        for(int i = 0; i< circleButtonLayoutAdapter.getCount(); i++) {
            final View view = circleButtonLayoutAdapter.getViewAt(i);
            String name = circleButtonLayoutAdapter.getNameAt(i);
            TextView buttonName = view.findViewById(R.id.button_name);
            buttonName.setText(name);
            RelativeLayout button = view.findViewById(R.id.custom_bt_layout);
            button.setOnClickListener(this);
            button.setTag(name);
            button.setId(i);
        }
        //Set time for today's date
        Calendar c;
        c = Calendar.getInstance();
        c.clear(Calendar.AM_PM);
        c.clear(Calendar.MINUTE);
        c.clear(Calendar.SECOND);
        c.clear(Calendar.MILLISECOND);
        c.set(Calendar.HOUR_OF_DAY,0);
        TextView dateText = backgroundView.findViewById(R.id.date_text_view);
        currDate = c.getTime();
        displayedDay = Integer.parseInt(dayFormatter.format(currDate));
        displayedMonth = Integer.parseInt(monthFormatter.format(currDate));
        displayedYear = Integer.parseInt(yearFormatter.format(currDate));
        dateText.setText(dateFormatter.format(currDate));
        dateInt = Integer.parseInt(intDateFormatter.format(currDate).toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.vitality_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent startSettingsActivity = new Intent(this, SettingsActivity.class);
            startActivity(startSettingsActivity);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
